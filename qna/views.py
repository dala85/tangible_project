from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required,user_passes_test
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from .models import Question,Answer,Chat,ChatRoom,Message,MessageAdmin



	
def qna_history(request):
	if request.method == "POST":
		question = request.POST.get('question')
		user = request.user
		question_id=Question.objects.create(question=question,user=user)
		return redirect('qna_history')
	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
	except:
		rooms = None 
	datas = Question.objects.order_by('-date')
	count = Question.objects.count()
	return render(request,'qna/qna.html',{'datas':datas ,'rooms':rooms,'count':count})


def user_question(request):
	if request.method == "POST":
		question = request.POST.get('question')
		user = request.user
		question_id=Question.objects.create(question=question,user=user)
		return redirect('qna_history')
	else:
		return redirect('qna_history')

	return render(request,'qna/user_question.html')

def user_history(request):
	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
		datas = Question.objects.order_by('date').filter(user=request.user)
	except:
		rooms = None
		datas = None
	
	return render(request,'qna/user_history.html',{'datas':datas ,'rooms':rooms})


@login_required
def vote(request, answer_obj):
	data = get_object_or_404(Answer,pk=answer_obj)
	data.likes.add(request.user)
	return HttpResponseRedirect("/#"+str(answer_obj))
	return render(request,'qna/vote.html',{"data":data})



def chat(request):
	if request.method == "POST":
		message = request.POST.get('message')
		user = request.user
		message_id=Chat.objects.create(message=message,user=user)
		return redirect('chat')
	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
	except:
		rooms = None
	datas = Chat.objects.order_by('-date')
	return render(request,'qna/chat.html',{'datas':datas ,'rooms':rooms})


def users_list(request):
	user = User.objects.all()
	users = user.exclude(username='admin')
	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
	except:
		rooms = None
	return render(request,'qna/users_list.html',{'users':users,'rooms':rooms})

@login_required
def create_chat_room(request,username):
	users_2 = User.objects.get(username=username)
	if ChatRoom.objects.filter(name=username+' & '+str(request.user)).exists() or ChatRoom.objects.filter(name=str(request.user)+' & '+username).exists():
		return redirect('qna_history')
	else:

		chat = ChatRoom.objects.create(name=username+' & '+str(request.user))
		greeting = Message.objects.create(chatroom=chat,text='This is the very beginning of direct message history' ,user=request.user)
		chat.participant.add(request.user) 
		chat.participant.add(users_2) 
		return redirect('qna_history')

	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
	except:
		rooms = None

	return render(request,'qna/create_chat_room.html',{'rooms':rooms})

def user_chat(request,chat_id):
	room = get_object_or_404(ChatRoom,pk=chat_id)
	if request.method == "POST":
		message = request.POST.get('message')
		message_id=Message.objects.create(text=message,user=request.user,chatroom=room)
		return HttpResponseRedirect("/user_chat/"+str(chat_id))
	room = get_object_or_404(ChatRoom,pk=chat_id)
	datas = room.messages.all()
	try:
		rooms= ChatRoom.objects.all().filter(participant=request.user)
		datas = room.messages.all().order_by('-date')
	except:
		rooms = None
		datas = None
	return render(request,'qna/user_chat.html',{'datas':datas,'rooms':rooms})


@login_required
def message_admin_help(request):
	if request.method == "POST":
		message = request.POST.get('message')
		email = request.POST.get('email')
		message_admin_help=MessageAdmin.objects.create(message=message,user=request.user,email=email)
		return redirect('message_admin_help')
	else:
		return redirect('qna_history')
	return render(request,'qna/message_admin_help.html')
    
