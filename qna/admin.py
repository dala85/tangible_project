from django.contrib import admin
from .models import Question,Answer,Chat,ChatRoom,Message,MessageAdmin


admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Chat)
admin.site.register(ChatRoom)
admin.site.register(Message)
admin.site.register(MessageAdmin)